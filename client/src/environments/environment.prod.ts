import { secrets } from './secrets';

export const environment = {
  production: true,
  projectRef: "xdnudhstgqmbdugrygwh",
  supabaseUrl: secrets.supabaseUrl,
  supabaseKey: secrets.supabaseKey,
};
