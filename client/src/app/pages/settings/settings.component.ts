import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyInfo } from 'src/app/models/CompanyInfo';
import { SupabaseService } from 'src/app/services/supabase/supabase.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
  company_info: CompanyInfo | null = null;

  constructor(private readonly supabase: SupabaseService, private router: Router) {}

  async signOut() {
    await this.supabase.signOut();
    this.router.navigate(['/']);
  }
}
