import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SupabaseService } from 'src/app/services/supabase/supabase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loading = false;

  constructor(
    private readonly supabase: SupabaseService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.supabase.session) {
      this.router.navigate(['/dashboard']);
    }
  }

  async onLogin(input: string) {
    try {
      this.loading = true;
      await this.supabase.signIn(input);
      //todo: show success message
      alert('Check your email for the login link!');
    } catch (error: any) {
      alert(error.error_description || error.message);
    } finally {
      this.loading = false;
    }
  }
}
