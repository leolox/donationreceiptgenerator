import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestTableComponent } from './components/test-table/test-table.component';
import { SessionGuard } from './guards/session/session.guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { ImprintComponent } from './pages/imprint/imprint.component';
import { LoginComponent } from './pages/login/login.component';
import { SettingsComponent } from './pages/settings/settings.component';

const routes: Routes = [
  /**
   * public routes
   */
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'test', component: TestTableComponent },
  { path: 'imprint', component: ImprintComponent },

  /**
   * protected routes
   */
  {
    path: 'settings',
    canActivate: [SessionGuard],
    component: SettingsComponent,
  },
  {
    path: 'dashboard',
    canActivate: [SessionGuard],
    component: DashboardComponent,
  },

  /**
   * fallback
   */
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabledBlocking',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
