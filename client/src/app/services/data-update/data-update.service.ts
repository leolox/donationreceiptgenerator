import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataUpdateService {

  public subject = new BehaviorSubject<any>(0);
  public obs = this.subject.asObservable();

  constructor() { }
}
