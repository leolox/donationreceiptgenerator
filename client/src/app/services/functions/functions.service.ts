import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReceiptActionType } from 'src/app/models/receiptAction';
import { ReceiptVariables } from 'src/app/models/RequestReceipt';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FunctionsService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: 'Bearer ' + environment.supabaseKey,
  });
  constructor(private http: HttpClient) {}

  callTest() {
    const body = {
      name: 'World',
    };
    const options = {
      headers: this.headers,
    };

    this.http
      .post(
        `https://${environment.projectRef}.functions.supabase.co/hello-world`,
        body,
        options
      )
      .subscribe((response) => console.log(response));
  }

  /**
   *
   * @param id - owner company id
   * @param variables - variables to fill in the form
   * @param action - download, save, save && email
   * @returns todo:
   */
  callGenerateReceipt(
    id: string,
    variables: ReceiptVariables,
    action: ReceiptActionType
  ): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      let responseType: string;
      if (action === ReceiptActionType.DOWNLOAD_ONLY) {
        responseType = 'blob';
      } else {
        responseType = 'json';
      }
      const body = {
        ownerId: id,
        variables,
        action,
      };
      const options = {
        headers: this.headers,
        responseType: responseType as 'json',
      };
      console.log(body);
      this.http
        .post(
          //`http://localhost:8000`,
          `https://donationhustle.com:443/functions/v1/`,
          //
          //`https://${environment.projectRef}.functions.supabase.co/generateReceipt`,
          //`http://localhost:54321/functions/v1/`,
          body,
          options
        )
        .subscribe((response) => {
          console.log(response);
          resolve(response);
        });
    });
  }
}
