import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  AuthChangeEvent,
  createClient,
  Session,
  SupabaseClient,
} from '@supabase/supabase-js';
import { Profile } from 'src/app/models/Profile';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SupabaseService {
  private supabase: SupabaseClient;
  public client: SupabaseClient | null = null;

  constructor(private router: Router) {
    this.supabase = createClient(
      environment.supabaseUrl,
      environment.supabaseKey
    );
    this.client = this.supabase;

    this.supabase.auth.onAuthStateChange(async (event, session) => {
      if (event === 'SIGNED_IN') {
        const result = await this.supabase
          .from('profiles')
          .select(`m_orgs`)
          .eq('id', this.user?.id)
          .single();
        console.log('SIGNED_IN', result);
        if (result.error) {
          console.log('updateProfile!');
          //create profile entry
          const t = await this.updateProfile({
            username: null,
            website: null,
            avatar_url: null,
          });
          console.log('updateProfile', t);
          console.log('no m_orgs');

          await this.crateOrganization();
          this.router.navigate(['/settings']);
        }

      }
      if (event === 'SIGNED_OUT') {
      }
    });
  }

  get user() {
    return this.supabase.auth.user();
  }

  get session() {
    return this.supabase.auth.session();
  }

  get profile() {
    return this.supabase
      .from('profiles')
      .select(`username, website, avatar_url, m_orgs`)
      .eq('id', this.user?.id)
      .single();
  }

  async crateOrganization() {
    const result = await this.supabase.from('company_info').insert({}).single();

    const updateProfile = await this.supabase
      .from('profiles')
      .update({
        m_orgs: [result.body.id],
      })
      .eq('id', this.user?.id)
      .single();
  }

  authChanges(
    callback: (event: AuthChangeEvent, session: Session | null) => void
  ) {
    return this.supabase.auth.onAuthStateChange(callback);
  }

  signIn(email: string) {
    return this.supabase.auth.signIn({ email });
  }

  signOut() {
    return this.supabase.auth.signOut();
  }

  updateProfile(profile: Profile) {
    const update = {
      ...profile,
      id: this.user?.id,
      updated_at: new Date(),
    };

    return this.supabase.from('profiles').upsert(update, {
      returning: 'minimal', // Don't return the value after inserting
    });
  }

  downLoadImage(path: string, bucket: string) {
    return this.supabase.storage.from(bucket).download(path);
  }

  uploadAvatar(filePath: string, file: File) {
    return this.supabase.storage.from('avatars').upload(filePath, file);
  }
}
