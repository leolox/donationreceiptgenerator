import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { SupabaseService } from 'src/app/services/supabase/supabase.service';

@Component({
  selector: 'app-company-settings',
  templateUrl: './company-settings.component.html',
  styleUrls: ['./company-settings.component.scss'],
})
export class CompanySettingsComponent implements OnInit {
  loading = false;
  uploading = false;

  companyForm: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    signature_location: ['', Validators.required],
    website: [''],
    tel: [''],
    note: [''],
  });

  logo_url: SafeResourceUrl | undefined;
  base_pdf: SafeResourceUrl | undefined;
  signature_url: SafeResourceUrl | undefined;

  companyInfo: any;

  constructor(
    private readonly supabase: SupabaseService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private readonly dom: DomSanitizer
  ) {}

  async ngOnInit(): Promise<void> {
    this.companyInfo = await this.getCompanyInfo();
    this.fillForm();
  }

  onSubmit() {
    this.updateCompanyInfo();
  }

  async getCompanyInfo() {
    //@ts-ignore
    const { data, error } = await this.supabase.client
      ?.from('company_info')
      .select('*')
      .match({
        id: (await this.supabase.profile).body.m_orgs[0],
      })
      .single();

    //download files
    if (data.signature_url) {
      this.signature_url = await this.downloadFile(
        'company-info',
        data.signature_url
      );
    }

    if (data.logo_url) {
      this.logo_url = await this.downloadFile('company-info', data.logo_url);
    }
    if (data.base_pdf) {
      //@ts-ignore
      const { data: url, error } = await this.supabase.client?.storage
        .from('company-info')
        .createSignedUrl(data.base_pdf, 60 * 60 * 10);
      console.log(this.base_pdf);

      this.base_pdf = url.signedURL;
    }

    return data;
  }

  async updateCompanyInfo(customData?: any) {
    let newValues = {};
    if (customData) {
      newValues = customData;
    } else {
      newValues = this.companyForm.value;
    }

    this.loading = true;
    //@ts-ignore
    const { data, error } = await this.supabase.client
      .from('company_info')
      .update(newValues)
      .match({
        id: (await this.supabase.profile).body.m_orgs[0],
      });
    if (error) {
      this.snackBar.open(error.message, '', { duration: 3000 });
      this.loading = false;
    }
    if (data) {
      this.snackBar.open('Successfully updated', '', { duration: 3000 });
      this.loading = false;
    }
  }

  fillForm() {
    this.companyForm.patchValue({
      name: this.companyInfo.name,
      signature_location: this.companyInfo.signature_location,
      website: this.companyInfo.website,
      tel: this.companyInfo.tel,
      note: this.companyInfo.note,
    });
  }

  async uploadFile(
    event: any,
    bucket: string,
    path: string
  ): Promise<string | undefined> {
    try {
      this.uploading = true;
      if (!event!.target.files || event.target.files.length === 0) {
        throw new Error('You must select an image to upload.');
      }

      const file = event.target.files[0];
      const fileExt = file.name.split('.').pop();
      const fileName = file.name;

      const filePath = `${path}/${fileName}`;
      await this.supabase.client!.storage.from(bucket).upload(filePath, file);

      return filePath;
    } catch (error: any) {
      alert(error.message);
      return undefined;
    } finally {
      this.uploading = false;
    }
  }

  async downloadFile(
    bucket: string,
    path: string
  ): Promise<SafeResourceUrl | undefined> {
    try {
      const { data, error } = await this.supabase
        .client!.storage.from(bucket)
        .download(path);
      const result = this.dom.bypassSecurityTrustResourceUrl(
        URL.createObjectURL(data!)
      );
      return result;
    } catch (error: any) {
      console.error('Error downloading image: ', error.message);
      return undefined;
    }
  }

  async uploadSignature(event: any) {
    const filePath = await this.uploadFile(
      event,
      'company-info',
      `${(await this.supabase.profile).body.m_orgs[0]}/signatures`
    );
    if (filePath) {
      this.signature_url = await this.downloadFile('company-info', filePath);
      this.updateCompanyInfo({ signature_url: filePath });
    }
  }

  async uploadLogo(event: any) {
    const filePath = await this.uploadFile(
      event,
      'company-info',
      `${(await this.supabase.profile).body.m_orgs[0]}/logos`
    );
    if (filePath) {
      this.logo_url = await this.downloadFile('company-info', filePath);
      this.updateCompanyInfo({ logo_url: filePath });
    }
  }

  async uploadBasePdf(event: any) {
    const filePath = await this.uploadFile(
      event,
      'company-info',
      `${(await this.supabase.profile).body.m_orgs[0]}/base_pdf`
    );
    if (filePath) {
      this.base_pdf = await this.downloadFile('company-info', filePath);
      this.updateCompanyInfo({ base_pdf: filePath });
    }
  }
}
