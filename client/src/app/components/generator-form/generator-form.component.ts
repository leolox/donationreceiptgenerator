import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReceiptActionType } from 'src/app/models/receiptAction';
import { DataUpdateService } from 'src/app/services/data-update/data-update.service';
import { FunctionsService } from 'src/app/services/functions/functions.service';
import { SupabaseService } from 'src/app/services/supabase/supabase.service';

@Component({
  selector: 'app-generator-form',
  templateUrl: './generator-form.component.html',
  styleUrls: ['./generator-form.component.scss'],
})
export class GeneratorFormComponent implements OnInit {
  generatorForm: FormGroup = this.formBuilder.group({
    company: ['', Validators.required],
    firstName: ['', [Validators.required, Validators.minLength(2)]],
    lastName: ['', [Validators.required, Validators.minLength(2)]],
    email: ['', [Validators.required, Validators.email]],
    address: ['', [Validators.required, Validators.minLength(2)]],
    amount: [null, [Validators.required, Validators.min(0)]],
    currency: ['', [Validators.required]],
    date: [null, Validators.required],
    renounce: [false, Validators.required],
  });

  constructor(
    private formBuilder: FormBuilder,
    private functions: FunctionsService,
    private readonly supabase: SupabaseService,
    private dataUpdateService: DataUpdateService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}

  async onSubmit(): Promise<void> {
    if (this.generatorForm.invalid) {
      return;
    }
    const companyId = await this.getCompanyId();
    const companyInfo = await this.getDetails(companyId);

    const result = await this.functions.callGenerateReceipt(
      companyId,
      {
        signature_date: new Date().toDateString(),
        ...companyInfo,
        ...this.generatorForm.value,
      },
      ReceiptActionType.SAVE_AND_SEND_EMAIL
    );
    console.log(result);
    this.dataUpdateService.subject.next(1);
    this.snackBar.open('Receipt sent', '', {
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }

  async onDownload(): Promise<void> {
    if (this.generatorForm.invalid) {
      this.generatorForm.markAllAsTouched();
      return;
    }
    const companyId = await this.getCompanyId();
    const companyInfo = await this.getDetails(companyId);

    const blob = await this.functions.callGenerateReceipt(
      companyId,
      {
        signature_date: new Date().toDateString(),
        ...companyInfo,
        ...this.generatorForm.value,
      },
      ReceiptActionType.DOWNLOAD_ONLY
    );
    const url = URL.createObjectURL(blob);
    window.open(url, '_blank');
  }

  async onSaveList(): Promise<void> {
    if (this.generatorForm.invalid) {
      this.generatorForm.markAllAsTouched();
      return;
    }
    const companyId = await this.getCompanyId();
    const companyInfo = await this.getDetails(companyId);

    const result = await this.functions.callGenerateReceipt(
      companyId,
      {
        signature_date: new Date().toDateString(),
        ...companyInfo,
        ...this.generatorForm.value,
      },
      ReceiptActionType.SAVE_ONLY
    );
    console.log(result);
    this.dataUpdateService.subject.next(1);
    this.snackBar.open('saved in list', '', {
      duration: 2000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }

  private async getCompanyId(): Promise<string> {
    return (await this.supabase.profile).body.m_orgs[0];
  }

  private async getDetails(companyId: string): Promise<any> {
    const companyInfo = await this.supabase.client
      ?.from('company_info')
      .select('signature_location, signature_url')
      .match({ id: companyId })
      .single();

    const signature_url = (
      await this.supabase.client?.storage
        .from('company-info')
        .createSignedUrl(companyInfo?.body.signature_url, 120)
    )?.signedURL;

    const result = {
      signature_location: companyInfo?.body.signature_location,
      signature_url,
    };
    return result;
  }
}
