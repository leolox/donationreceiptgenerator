import { HttpClient } from '@angular/common/http';
import { Component, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort, MatSortable, SortDirection } from '@angular/material/sort';
import {
  BehaviorSubject,
  from,
  merge,
  Observable,
  of as observableOf,
  of,
  Subscription,
} from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Receipt } from 'src/app/models/Receipt';
import { ReceiptActionType } from 'src/app/models/receiptAction';
import { DataUpdateService } from 'src/app/services/data-update/data-update.service';
import { FunctionsService } from 'src/app/services/functions/functions.service';
import { SupabaseService } from 'src/app/services/supabase/supabase.service';

//https://github.com/angular/components/issues/13431

@Component({
  selector: 'app-receipt-table',
  templateUrl: './receipt-table.component.html',
  styleUrls: ['./receipt-table.component.scss'],
})
export class ReceiptTableComponent implements AfterViewInit, OnDestroy {
  displayedColumns: string[] = [
    'created_at',
    'company',
    'firstName',
    'lastName',
    'address',
    'amount',
    'date',
    'renounce',
    'email',
    'actions',
  ];
  exampleDatabase: ExampleHttpDatabase | null = null;
  data: Receipt[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  subscriptions: Subscription[] = [];

  //@ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  //@ts-ignore
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _httpClient: HttpClient,
    private supabase: SupabaseService,
    private dataUpdateService: DataUpdateService,
    private snackBar: MatSnackBar,
    private functions: FunctionsService
  ) {}

  ngAfterViewInit() {
    this.exampleDatabase = new ExampleHttpDatabase(
      this._httpClient,
      this.supabase
    );

    this.subscriptions.push(
      this.dataUpdateService.obs.subscribe(() => {
        if (!this.isLoadingResults) {
          this.sort.sort({ id: 'created_at', start: 'desc' } as MatSortable);
        }
      })
    );

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.exampleDatabase!.getReceipts(
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex
          ).pipe(catchError(() => observableOf(null)));
        }),
        map((data) => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;

          if (data === null) {
            return [];
          }

          // Only refresh the result length if there is new data. In case of rate
          // limit errors, we do not want to reset the paginator to zero, as that
          // would prevent users from re-triggering requests.
          this.resultsLength = data.total_count;
          return data.items;
        })
      )
      .subscribe((data) => (this.data = data));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onClick(a?: any) {
    console.log('A', a);
  }

  async onSendClick(event: Event, values: any) {
    event.preventDefault();
    event.stopPropagation();
    console.log('values', values);

    const companyId = (await this.supabase.profile).body.m_orgs[0];
    const action = ReceiptActionType.SEND_ONLY_EMAIL;
    const result = await this.functions.callGenerateReceipt(
      companyId,
      values,
      action
    );

    if (result.error) {
      this.snackBar.open('oh now something went wrong 😫', '', {
        duration: 5000,
      });
    }
    this.snackBar.open('Receipt sent!', 'ok', {
      duration: 5000,
    });
  }

  async onDownloadClick(event: Event, values: any) {
    event.preventDefault();
    event.stopPropagation();

    //@ts-ignore
    const { data: pdfBlob, error } = await this.supabase.client?.storage
      .from('receipts')
      .download(values.receipts_pdf_url);

    if (error) {
      console.log(error);
      this.snackBar.open('oh now something went wrong 😫', 'OK', {
        duration: 5000,
      });
    }
    const blob = new Blob([pdfBlob], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }
}

export interface ApiWrapper {
  items: Receipt[];
  total_count: number;
}

/** An example database that the data source uses to retrieve data for the table. */
export class ExampleHttpDatabase {
  constructor(
    private _httpClient: HttpClient,
    private supabase: SupabaseService
  ) {}

  getReceipts(
    sort: string,
    order: SortDirection,
    page: number
  ): Observable<ApiWrapper> {
    return new Observable<ApiWrapper>((observer) => {
      const ascending = order === 'asc' ? true : false;

      //const owner = (await this.supabase.profile).body.m_orgs[0];
      this.supabase.profile.then((res) => {
        const owner = res.body.m_orgs[0];
        console.log('owner', owner);

        const query = this.supabase
          .client!.from('receipts')
          .select(
            `
  *,
  receipts_company:receipts-company (
    *
  )
`,
            { count: 'estimated' }
          )
          .match({
            owned_by: owner,
          })
          .order(sort, { ascending }) //.order('id', { ascending: false })
          .range(0, 30);

        query.then((data) => {
          console.log('data', data);
          const result = { items: data.body!, total_count: data.count! };
          observer.next(result);
          observer.complete();
        });
      });
    });
  }

  /**
  *
  * Example implementation of the database.
  
  getRepoIssues(
    sort: string,
    order: SortDirection,
    page: number
  ): Observable<GithubApi> {
    this.getReceipts('id', 'asc', 0);

    const href = 'https://api.github.com/search/issues';
    const requestUrl = `${href}?q=repo:angular/components&sort=${sort}&order=${order}&page=${
      page + 1
    }`;

    return this._httpClient.get<GithubApi>(requestUrl);
  }
   */
}
