import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Profile } from 'src/app/models/Profile';
import { SupabaseService } from 'src/app/services/supabase/supabase.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent implements OnInit {
  profileLoading = false;
  profile: Profile | undefined;
  _avatarUrl: SafeResourceUrl | undefined;
  _companyLogoUrl: SafeResourceUrl | undefined;
  constructor(
    private readonly supabase: SupabaseService,
    private readonly dom: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.getProfile();
  }

  async getProfile() {
    try {
      this.profileLoading = true;
      let { data: profile, error, status } = await this.supabase.profile;

      if (error && status !== 406) {
        throw error;
      }

      if (profile) {
        this.profile = profile;

        this.getCompanyInfo(profile.m_orgs[0]);

        this._avatarUrl = await this.downloadImage(
          profile.avatar_url,
          'avatars'
        );
      }
    } catch (error: any) {
      alert(error.message);
    } finally {
      this.profileLoading = false;
    }
  }

  async getCompanyInfo(id: string) {
    //@ts-ignore
    const { data: company_info, error } = await this.supabase.client
      ?.from('company_info')
      .select('*')
      .eq('id', id)
      .single();

    this._companyLogoUrl = await this.downloadImage(
      company_info.logo_url,
      'public/company-info'
    );
  }

  async downloadImage(path: string, bucket: string) {
    try {
      const { data } = await this.supabase.downLoadImage(path, bucket);
      const result = this.dom.bypassSecurityTrustResourceUrl(
        URL.createObjectURL(data!)
      );
      return result;
    } catch (error: any) {
      console.error('Error downloading image: ', error.message);
      return undefined;
    }
  }
}
