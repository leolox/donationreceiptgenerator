export interface Receipt {
    id: string,
    parent_company: string,
    owned_by: string,
    created_at: string,
    company: string,
    firstName: string,
    lastName: string,
    address: string,
    amount: number,
    date: string,
    renounce: boolean,
    email: string,
    signature_location: string,
    signature_date: string,
    signature_url: string,
    receipts_company: any
}