import { ReceiptActionType } from "./receiptAction";

export interface RequestReceipt { 
  ownerId: string;
  action: ReceiptActionType;
  variables: ReceiptVariables;
}

export interface ReceiptVariables {
  company: string;
  firstName: string;
  lastName: string;
  address: string;
  amount: string;
  date: string;
  renounce: false;
  email: string;
  signature_location: string;
  signature_date: string;
  signature_url: string;
}
