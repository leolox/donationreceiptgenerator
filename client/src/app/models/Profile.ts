export interface Profile {
    username: string | null| undefined;
    website: string | null| undefined;
    avatar_url: string | null | undefined;
  }