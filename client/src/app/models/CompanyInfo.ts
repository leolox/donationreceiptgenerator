export interface CompanyInfo {
  id: string;
  created_at?: string;
  name: string;
  website?: string;
  tel?: string;
  note?: string;
  logo_url?: string;
}
