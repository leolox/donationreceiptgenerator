// Follow this setup guide to integrate the Deno language server with your editor:
// https://deno.land/manual/getting_started/setup_your_environment
// This enables autocomplete, go to definition, etc.

import { serve } from "https://deno.land/std@0.131.0/http/server.ts";
import { corsHeaders } from "../_shared/cors.ts";
import { PDFDocument } from "https://cdn.skypack.dev/pdf-lib@^1.11.1?dts";
import { supabaseClient } from "../_shared/supabaseClient.ts";
import { ReceiptActionType } from "../models/receiptAction.ts";
import { SMTPClient } from "https://deno.land/x/denomailer/mod.ts";
import { config as dotEnvConfig } from "https://deno.land/x/dotenv@v1.0.1/mod.ts";

console.log("function:generateReceipt UP🚀");

dotEnvConfig({ export: true });

const smtpConfig = {
  hostname: Deno.env.get("SMTP_HOST") ?? "",
  port: parseInt(Deno.env.get("SMTP_PORT") ?? "25"),
  tls: false, //upgrades to STARTTLS
  auth: {
    username: Deno.env.get("SMTP_USER") ?? "",
    password: Deno.env.get("SMTP_PASS") ?? "",
  },
};

serve(async (req: Request) => {
  if (req.method === "OPTIONS") {
    return new Response("ok", { headers: corsHeaders });
  }

  console.log("start");

  const { ownerId, companyId, variables, action } = await req.json();

  //no need to generate receipt if already in bucket
  if (action === ReceiptActionType.SEND_ONLY_EMAIL) {
    const pdfPathQuery = await supabaseClient
      .from("receipts")
      .select("receipts_pdf_url")
      .match({ id: variables.id })
      .single();

    const resultEmail = await sendEmail(
      variables.email,
      pdfPathQuery.body.receipts_pdf_url
    );
    return new Response(
      JSON.stringify({
        status: "ok",
        action: ReceiptActionType.SEND_ONLY_EMAIL,
        data: { resultEmail },
      }),
      {
        headers: { ...corsHeaders, "Content-Type": "application/json" },
        status: 200,
      }
    );
  }

  const basePdfPath = (
    await supabaseClient
      .from("company_info")
      .select(`base_pdf`)
      .eq("id", ownerId)
      .single()
  ).body?.base_pdf;

  const { data: customBasePdfBlob, error: storageErr } =
    await supabaseClient.storage.from("company-info").download(basePdfPath);

  let basePdfBytes;
  if (customBasePdfBlob) {
    basePdfBytes = await customBasePdfBlob.arrayBuffer();
  } else {
    //default base pdf
    const basePdfUrl =
      "https://xdnudhstgqmbdugrygwh.supabase.co/storage/v1/object/public/public/defaultPdf/example_form.pdf";
    /**
     * fetch resources
     */
    basePdfBytes = await fetch(basePdfUrl).then((res) => res.arrayBuffer());
  }

  // Fetch the signature image
  const signatureImageBytes = await fetch(variables.signature_url).then((res) =>
    res.arrayBuffer()
  );

  /**
   * Load a PDF with form fields
   */
  const pdfDoc = await PDFDocument.load(basePdfBytes);

  /**
   * embed images
   */
  const signatureImage = await pdfDoc.embedPng(signatureImageBytes);

  // Get the form containing all the fields
  const form = pdfDoc.getForm();

  /**
   * Get all fields by their names
   */
  const company = form.getTextField("company");
  const firstName = form.getTextField("firstName");
  const lastName = form.getTextField("lastName");
  const address = form.getTextField("address");
  const amount = form.getTextField("amount");
  const amountLetter = form.getTextField("amountLetter");
  //const currency = form.getTextField("currency"); //todo: add currency
  const date = form.getTextField("date");
  const renounce_yes = form.getCheckBox("renounce_yes");
  const renounce_no = form.getCheckBox("renounce_no");
  const signature_location = form.getTextField("signature_location");
  const signature_date = form.getTextField("signature_date");

  const signature = form.getButton("signature");

  /**
   * Fill in the fields
   */

  company.setText(variables.company);
  firstName.setText(variables.firstName);
  lastName.setText(variables.lastName);
  address.setText(variables.address);
  amount.setText(variables.amount.toString());
  amountLetter.setText(variables.amountLetter);
  //currency.setText(variables.currency);
  date.setText(variables.date);
  if (variables.renounce) {
    renounce_yes.check();
  } else {
    renounce_no.check();
  }
  signature_location.setText(variables.signature_location);
  signature_date.setText(variables.signature_date);

  // fill image
  signature.setImage(signatureImage);

  form.flatten(); //aka. flatten the form to a single PDF page

  const pdfBytes = await pdfDoc.save();

  /**
   *
   * RESPONSE
   *
   */

  if (action === ReceiptActionType.DOWNLOAD_ONLY) {
    return new Response(pdfBytes, {
      headers: { ...corsHeaders, "Content-Type": "application/pdf" },
      status: 200,
    });
  }

  if (action === ReceiptActionType.SAVE_ONLY) {
    const receiptId = await saveInDb(ownerId, variables, pdfBytes, companyId);

    return new Response(
      JSON.stringify({
        status: "ok",
        action: ReceiptActionType.SAVE_ONLY,
        data: { receiptId },
      }),
      {
        headers: { ...corsHeaders, "Content-Type": "application/json" },
        status: 200,
      }
    );
  }

  if (action === ReceiptActionType.SAVE_AND_SEND_EMAIL) {
    const saveResult = await saveInDb(ownerId, variables, pdfBytes, companyId);
    const resultEmail = await sendEmail(variables.email, saveResult.pdfPath);
    return new Response(
      JSON.stringify({
        status: "ok",
        action: ReceiptActionType.SAVE_AND_SEND_EMAIL,
        data: { saveResult, resultEmail },
      }),
      {
        headers: { ...corsHeaders, "Content-Type": "application/json" },
        status: 200,
      }
    );
  }

  return new Response(JSON.stringify({ status: "ok" }), {
    headers: { ...corsHeaders, "Content-Type": "application/json" },
    status: 200,
  });
});

/**
 *
 * @param ownerId
 * @param pdfBytes
 * @param variables
 * @param companyId
 * @returns - receipt id
 */
async function saveInDb(
  ownerId: string,
  variables: any, //todo: typesave
  pdfBytes: any | undefined,
  companyId?: string
): Promise<any> {
  /**
   * create receipt-company on the fly
   */
  if (!companyId) {
    const preResult = await supabaseClient
      .from("receipts-company")
      .insert({
        owned_by: ownerId,
        company_name: variables.company,
        firstName: variables.firstName,
        lastName: variables.lastName,
        address: variables.address,
        email: variables.email,
      })
      .single();
    if (preResult.error) {
      return { result: preResult.error };
    }
    companyId = preResult.data.id;
  }

  /**
   * save receipt
   */
  const result = await supabaseClient
    .from("receipts")
    .insert({
      owned_by: ownerId,
      parent_company: companyId,
      ...variables,
    })
    .single();
  if (result.error) {
    return { result: result.error };
  }

  /**
   * upload pdf to storage
   */

  //todo: test in prod

  const myPdfPath = `${ownerId}/${companyId}/${result.body.id}.pdf`;

  const { data: _storageResult, error: storageErr } =
    await supabaseClient.storage.from("receipts").upload(myPdfPath, pdfBytes);

  if (storageErr) {
    console.log(storageErr);
  }

  //fix path
  await supabaseClient
    .from("receipts")
    .update({
      receipts_pdf_url: myPdfPath,
    })
    .match({ id: result.body.id });

  return { result: result.body.id, pdfPath: myPdfPath };
}

async function sendEmail(email: string, pdfPath: string) {
  const client = new SMTPClient({
    connection: smtpConfig,
  });

  const { data: pdfBlob } = await supabaseClient.storage
    .from("receipts")
    .download(pdfPath);

  const attachment: any = {
    encoding: "base64",
    contentType: "application/pdf",
    filename: `${new Date().getTime()}_donation_receipt.pdf`,
    content: await blobToBase64(pdfBlob),
  };

  await client.send({
    from: "noreply@lox.de",
    to: email,
    subject: "Donation receipt",
    content: "Donation receipt",
    html: `
    <h1>Donation receipt</h1>
    <p>Check the attachment</p>
    `,
    attachments: [attachment],
  });

  await client.close();

  return true;
}

function blobToBase64(blob: any): Promise<any> {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.readAsDataURL(blob);
  });
}

// To invoke:
// curl -i --location --request POST 'http://localhost:54321/functions/v1/' \
//   --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6ImFub24ifQ.625_WdcF3KHqz5amU0x2X5WWHP-OEs_4qj0ssLNHzTs' \
//   --header 'Content-Type: application/json' \
//   --data '{"name":"Functions"}'
